describe('your Vue app', () => {
  beforeAll(async () => {
    await page.goto('http://52.156.77.163:8080');
  });

  it('this is testable with jest and puppeteer then it checks title tag', async () => {
    await expect(page).toMatchElement('title','TodoApp ClientSide')

  })
  it('this is testable with jest and puppeteer then it checks if "All Todos" text exists', async () => {
    await expect(page).toMatchElement('.todos-title','All Todos')
  })
  it('this is testable with jest and puppeteer then it adds a todo',async()=>{
    jest.setTimeout(20000);
    await page.type('.test-input','test todo e2e')
    await page.waitFor(4000)
    await expect(page).toClick('button', { text: 'Add Todo' })
    await page.waitFor(4000)
    await page.type('.test-input','test todo e2e 2')
    await page.waitFor(4000)
    await expect(page).toClick('button', { text: 'Add Todo' })
    await page.waitFor(4000)
  })
});
