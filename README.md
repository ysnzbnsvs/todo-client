# todo-client

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your e2e tests
```
npm run test:e2e
```

### Run your acceptance tests
```
npm run test:acceptance
```


### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### This project will serve at [this](http://52.156.77.163:8080/) address for a certain period of time
